import csv
import gc
import logging
import os
import re
from collections import Counter, defaultdict
from itertools import chain

import cPickle as pickle
import numpy as np
from nltk import WordNetLemmatizer

logger = logging.getLogger('vsm')
logger.setLevel(logging.DEBUG)
ch = logging.FileHandler("vsm.log", mode='a')
ch.setLevel(logging.DEBUG)
ch.setFormatter(logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s'))
logger.addHandler(ch)


def main(max_lines=88083626):
    logger.debug("Starting vector space model")

    # Load text
    path = os.path.dirname(os.path.abspath(__file__))
    corpus_path = os.path.join(path, "full.txt")
    simlex_path = os.path.join(path, "SimLex-999.csv")
    wordsim_path = os.path.join(path, "wordsim353.csv")

    # Pre-processing and loading common words
    pp = prepro()

    # Loads the most common words
    cw = load_data(corpus_path, pp, max_lines=max_lines, top=5000)
    # Loads words that needs to be looked for in the data, they are extraced from the CSV files.
    rel_words = load_relevant_words(simlex_path, wordsim_path, pp)
    # Training the models
    model = VSMModel(cw, rel_words, pp, max_lines)

    model.train(corpus_path)
    logger.debug("Finished training")

    wordsim_dict = load_wordsim(wordsim_path, pp)
    logger.debug("Obtained wordsim gold standard")

    logger.debug("PPMi Model with window of 1 sparsity of: " + str(sparsity(model.ppmi_matrix1)))
    logger.debug("PPMi Model with window of 2 sparsity of: " + str(sparsity(model.ppmi_matrix2)))
    logger.debug("freq Model with window of 1 sparsity of: " + str(sparsity(model.frequency_matrix1)))
    logger.debug("freq Model with window of 2 sparsity of: " + str(sparsity(model.frequency_matrix2)))

    logger.debug("Calculating model similarities")
    logger.debug("Calculating correlation vs wordsim gold standard")
    ppmi1_sim, freq1_sim, ppmi2_sim, freq2_sim = correlate(wordsim_dict, model)
    write_csv(path, "ppmi1", ppmi1_sim)
    write_csv(path, "freq1", freq1_sim)
    write_csv(path, "ppmi2", ppmi2_sim)
    write_csv(path, "freq2", freq2_sim)

    simlex_dict = load_simlex(simlex_path, pp)
    logger.debug("Obtained simlex gold standard")

    logger.debug("Calculating correlation vs simlex gold standard")
    full_simlex_dict = {}
    for k, v in simlex_dict.iteritems():
        full_simlex_dict.update(v)
        logger.debug("Spearman correlation for POS: " + str(k))
        correlate(v, model)

    # full simlex correlation
    logger.debug("Spearman correlation for all POS")
    correlate(full_simlex_dict, model)

    logger.debug("Finished vector space model")


def correlate(v, model):
    """
    Calculates the spearman correlation for all matrices and similarities
    :param v: dict<tuple<string,string>, float>, similarity dictionary for word tuples.
    :param model: VSMModel, a trained model
    :return: 4-tuple, similarity matrices (so they can be written to csv file)
    """
    correlation_calc = spearman
    ppmi1_sim, freq1_sim, ppmi2_sim, freq2_sim = model.get_similarities(v.keys())
    ppmi1_res = correlation_calc(v, ppmi1_sim)
    freq1_res = correlation_calc(v, freq1_sim)
    ppmi2_res = correlation_calc(v, ppmi2_sim)
    freq2_res = correlation_calc(v, freq2_sim)
    logger.debug("PPMi Model with window of 1 has a correlation of: " + str(ppmi1_res))
    logger.debug("freq Model with window of 1 has a correlation of: " + str(freq1_res))
    logger.debug("PPMi Model with window of 2 has a correlation of: " + str(ppmi2_res))
    logger.debug("freq Model with window of 2 has a correlation of: " + str(freq2_res))
    return ppmi1_sim, freq1_sim, ppmi2_sim, freq2_sim


def load_data(filepath, pp, max_lines=88083626, top=5000):
    """
    loads and pre-proccessing of data.

    :param filepath: String, the path to the file to parse.
    :param pp: function(string), pre-processing function
    :param max_lines:int, number of maximum lines to read from file
    :param top: int, number of highest frequency word to extract
    :return: list<string>, list of (top) most common words
    """
    # Loading persistence data
    if os.path.exists('common_words.pkl'):
        with open('common_words.pkl', 'rb') as input_:
            return pickle.load(input_)

    # initialization
    line_words = []
    pl = 0
    processed_per = 0
    factor = max_lines / 5000
    next_proc = 0
    word_counter = Counter()
    with open(filepath, "rb") as f:
        gc.disable()
        for line in f:
            # pre processing each line
            line_words.extend(pp(line))

            # for every 1% lines read we calculate their appearance
            pl += 1
            if pl >= next_proc:
                logger.debug("common words calculation - processed " + str(processed_per) + "% and total of " + str(pl))
                next_proc += factor
                processed_per += 0.02
                word_counter += Counter(line_words)
                gc.enable()
                line_words = []
                if pl >= max_lines:
                    break
                gc.disable()
        gc.enable()
        word_counter += Counter(line_words)
        cw = word_counter.most_common(top)

    # persistence
    with open('common_words.pkl', 'wb') as output:
        pickle.dump(cw, output, pickle.HIGHEST_PROTOCOL)
    return cw


def prepro():
    """
    Pre processing functions as defined in exercise instructions.
    :return: function, pre-process function that returns list<string> list of processed words
    """
    lemmy = WordNetLemmatizer()  # this is just a tribute
    delchars = ''.join(c for c in map(chr, range(256)) if not c.isalnum() and c != ' ')
    years = re.compile(r"\D\d{4}\D")
    nums = re.compile(r"\d+")

    def t(line):
        processed = line.lower().translate(None, delchars)
        processed = years.sub(" % ", processed)
        processed = nums.sub("#", processed).split()
        return map(lemmy.lemmatize, processed)

    return t


def write_csv(path, model_name, sim_dict):
    """
    Writes the csv file of word tuples and similarity
    :param path: string, path to directory to write the file in
    :param model_name: string, name of the model
    :param sim_dict: dict<tuple<string,string>, float> dictionary mapping word tuples to similarity scores.
    :return:
    """
    name = "comp_%s_200467918.csv" % model_name
    with open(os.path.join(path, name), "wb") as f:
        f.write("Word 1,Word 2,Score\r\n")
        for w_pair, sim in sim_dict.iteritems():
            f.write("%s,%s,%.2f\r\n" % (w_pair[0], w_pair[1], sim))


def load_relevant_words(simlex_path, wordsim_path, pp):
    """
    loads only relevant words for this task, this are the only words we will look into
    :param simlex_path: string, path to simlex file
    :param wordsim_path: string, path to wordsim file
    :param pp: function, pre processing function.
    :return: dict<str,int>, word->index dictionary
    """
    if os.path.exists('extracted_words.pkl'):
        with open('extracted_words.pkl', 'rb') as input_:
            return pickle.load(input_)
    rel_words = set()
    with open(simlex_path, "rb") as f:
        first = True
        for line in f:
            if first or line == "":
                first = False
                continue
            w1, w2, _, _ = line.split(",")
            rel_words.add(pp(w1)[0])
            rel_words.add(pp(w2)[0])

    with open(wordsim_path, "rb") as f:
        first = True
        for line in f:
            if first or line == "":
                first = False
                continue
            w1, w2, _ = line.split(",")
            rel_words.add(pp(w1)[0])
            rel_words.add(pp(w2)[0])
    extracted_words = dict([(j, i) for i, j in enumerate(rel_words)])
    with open('extracted_words.pkl', 'wb') as output:
        pickle.dump(extracted_words, output, pickle.HIGHEST_PROTOCOL)
    return extracted_words


def load_wordsim(filepath, pp):
    """
    Loads the wordsim353.csv file eand creates a dictionary of word tuples to similarity score.
    :param filepath: string, path to wordsim file.
    :param pp: function, pre processing function
    :return: dict<tuple<string,string>, float> dictionary of word tuples to similarity.
    """
    w1_key = 'Word 1'
    w2_key = 'Word 2'
    value_key = 'Score'
    sim_data = {}
    with open(filepath, "rb") as csvfile:
        reader = csv.DictReader(csvfile)
        for row in reader:
            w1 = str(pp(row[w1_key])[0])
            w2 = str(pp(row[w2_key])[0])
            sim = float(row[value_key])
            sim_data[(w1, w2)] = sim
    return sim_data


def load_simlex(filepath, pp):
    """
    Loads simlex-999.csv file
    :param filepath: string, path to simlex file
    :param pp:function, pre-processing function
    :return: dict<string,dict<tuple<string,string>, float>>, a dictionary of dictionaries mapping POS to dictionary of word tuples to similarity
    """
    w1_key = "word1"
    w2_key = "word2"
    value_key = "Score"
    pos = "POS"
    sim_data = defaultdict(dict)
    with open(filepath, "rb") as csvfile:
        reader = csv.DictReader(csvfile)
        for row in reader:
            w1 = str(pp(row[w1_key])[0])
            w2 = str(pp(row[w2_key])[0])
            sim = float(row[value_key])
            pk = str(row[pos])
            sim_data[pk][(w1, w2)] = sim
    return sim_data


def spearman(sim1, sim2):
    """
    Calculates spearman correlation between two dictionaries of similarity
    :param sim1: dict<tuple<string,string>, float>, similarity dictionary for word tuples.
    :param sim2: dict<tuple<string,string>, float>, similarity dictionary for word tuples.
    :return: float, spearman correlation value.
    """
    def rank(sim):
        rn = {}
        rank_num = 1
        for wp in sorted(sim, key=sim.get, reverse=True):
            rn[wp] = rank_num
            rank_num += 1

        for v in set(sim.itervalues()):
            share_value = [t for t in rn if sim[t] == v]
            num_share_value = len(share_value)
            if num_share_value > 1:
                new_rank = float(sum([rn[t] for t in share_value])) / num_share_value
                for wp in share_value:
                    rn[wp] = new_rank
        return rn

    rank1 = rank(sim1)
    rank2 = rank(sim2)
    n = len(rank1)

    s_sum = 0.0
    for (w_pair, r) in rank1.iteritems():
        d = r - rank2[w_pair]
        s_sum += d * d
    return 1 - 6 * s_sum / (n * (n * n - 1))


class VSMModel(object):
    """
    Vector space model implementation
    """
    def __init__(self, common_words, rel_words, preprocess_func, max_lines=88083626):
        """
        :param common_words: dict<string, int>, dictionary of strings of common words and their relative index in the matrices as columns
        :param rel_words: dict<string, int>, dictionary of strings to look for and their relative index in the matrices as rows
        :param preprocess_func: function, pre-processing function that takes a string and returnes a processed liust of strings
        :param max_lines: int (optional), number of lines of text to train from in the corpus
        :return:
        """
        self.rel_words = rel_words
        self.pp = preprocess_func
        self.cwi = dict((j[0], i) for i, j in enumerate(common_words))  # cwi: common words indexes
        self.dimension = (len(self.rel_words), len(self.cwi))
        self.frequency_matrix1 = np.zeros(self.dimension, dtype=np.int32)
        self.frequency_matrix2 = np.zeros(self.dimension, dtype=np.int32)
        self.ppmi_matrix1 = None
        self.ppmi_matrix2 = None
        self.max_lines = max_lines

    def load_from_persistency(self):
        """
        Loads pkl files for frequency matrices
        :return: boolean, true if files exists and loaded, false otherwise.
        """
        if not os.path.exists('frequency_matrix1.pkl') or not os.path.exists('frequency_matrix2.pkl'):
            return False

        with open('frequency_matrix1.pkl', 'rb') as input_:
            self.frequency_matrix1 = pickle.load(input_)
        with open('frequency_matrix2.pkl', 'rb') as input_:
            self.frequency_matrix2 = pickle.load(input_)
        self.ppmi_matrix1 = self.freq_ppmi(self.frequency_matrix1)
        self.ppmi_matrix2 = self.freq_ppmi(self.frequency_matrix2)
        return True

    def save_to_persistency(self):
        """
        Saves frequency matrices to pkl files
        :return:
        """
        with open('frequency_matrix1.pkl', 'wb') as output:
            pickle.dump(self.frequency_matrix1, output, pickle.HIGHEST_PROTOCOL)
        with open('frequency_matrix2.pkl', 'wb') as output:
            pickle.dump(self.frequency_matrix2, output, pickle.HIGHEST_PROTOCOL)

    # Builds matrix of frequency
    def train(self, file_path):
        """
        Passes over all words in the files, process them and counts frequencies, lastly creates ppmi matrices.
        Supports persistancy for frequency matrices, ppmi matrices are created on the fly.
        :param file_path: string, the path to the text file.
        :return:
        """
        if self.load_from_persistency():
            return
        else:
            max_lines = self.max_lines
            pl = 0
            factor = max_lines / 500
            notch = factor
            percent = 0
            line_list = []
            try:
                with open(file_path, "rb") as f:
                    for line in f:
                        pl += 1
                        line_list.append(line)
                        if pl >= notch:
                            for word_list in map(self.pp, line_list):
                                for w in filter(self.rel_words.get, word_list):
                                    # reversed the logic, first get words that are common, and then their index
                                    i = word_list.index(w)
                                    w_index = self.rel_words[w]

                                    # getting neighbors
                                    neighbors1_index = chain(xrange(max(0, i - 1), i), xrange(i + 1, min(i + 2, len(word_list))))
                                    neighbors2_index = chain(xrange(max(0, i - 2), i), xrange(i + 1, min(i + 3, len(word_list))))
                                    neighbors1 = [self.cwi.get(word_list[k]) for k in neighbors1_index if self.cwi.get(word_list[k]) is not None]
                                    neighbors2 = [self.cwi.get(word_list[k]) for k in neighbors2_index if self.cwi.get(word_list[k]) is not None]

                                    self.frequency_matrix1[w_index, neighbors1] += 1
                                    self.frequency_matrix2[w_index, neighbors2] += 1

                            line_list = []
                            percent += 0.2
                            notch += factor
                            logger.debug("Processed " + str(percent) + "% of the lines in train and total of " + str(pl))
                            if pl > max_lines:
                                break
            except KeyboardInterrupt:
                self.save_to_persistency()
                raise

            self.save_to_persistency()
            # build ppmi matrix
            self.ppmi_matrix1 = self.freq_ppmi(self.frequency_matrix1)
            self.ppmi_matrix2 = self.freq_ppmi(self.frequency_matrix2)

    @staticmethod
    def freq_ppmi(matrix):
        """
        Takes a frequency matrix and returns a ppmi matrix
        :param matrix: numpy.2dmatrix, matrix of frequency
        :return: numpy.2dmatrix, a ppmi score matrix
        """
        add2_mat = matrix + 2
        mat_sum = add2_mat.sum(dtype=np.float64)
        col_sum = np.matrix(add2_mat.sum(axis=0, dtype=np.float64))  # this is an array need to convert to matrix
        row_sum = np.matrix(add2_mat.sum(axis=1, dtype=np.float64)).T
        csrs = np.multiply(col_sum, row_sum)
        factor_matrix = mat_sum / csrs
        ppmi_matrix = np.log2(np.multiply(add2_mat, factor_matrix))
        ppmi_matrix[ppmi_matrix < 0] = 0
        return ppmi_matrix

    # For each combination of two words, the cosine value of the two vectors
    def get_similarities(self, wordpairs):
        """
        For a given list of word pairs (as tuples) gets the similarity scores using the matrices trained
        :param wordpairs: list<tuple<string,string>>, list of word tuples to look for
        :return: dict<tuple<string,string>,float>, dictionary of scores
        """
        # Choose the distance/similarity function
        # sim_func = pairwise_cosine
        sim_func = combo_dice_sim

        ppmi1_dist = sim_func(self.ppmi_matrix1)
        freq1_dist = sim_func(self.frequency_matrix1)
        ppmi2_dist = sim_func(self.ppmi_matrix2)
        freq2_dist = sim_func(self.frequency_matrix2)

        ppmi1_sim = {}
        freq1_sim = {}
        ppmi2_sim = {}
        freq2_sim = {}
        for (w1, w2) in wordpairs:
            if w1 in self.rel_words and w2 in self.rel_words:
                w1_index = self.rel_words[w1]
                w2_index = self.rel_words[w2]
                ppmi1_sim[(w1, w2)] = ppmi1_dist[w1_index, w2_index]
                freq1_sim[(w1, w2)] = freq1_dist[w1_index, w2_index]
                ppmi2_sim[(w1, w2)] = ppmi2_dist[w1_index, w2_index]
                freq2_sim[(w1, w2)] = freq2_dist[w1_index, w2_index]
        return ppmi1_sim, freq1_sim, ppmi2_sim, freq2_sim


# Basic models - cosine similarity
def pairwise_cosine(matrix):
    """
    Cosine similarity matrix
    :param matrix: numpy.2dmatrix, rows are "query words" and columns are "common words"
    :return:  numpy.2dmatrix, similarity matrix per "query words"
    """
    m = matrix.astype(np.float64)
    norms1 = np.linalg.norm(m, axis=1)
    cos_dist = np.dot(m, m[:].T)
    normalized = np.divide(cos_dist, norms1[:, None] * norms1)
    return normalized


def combo_dice_sim(matrix):
    """
    Our very own sim function, its a combination of dice similarity and biased matching non zero
    we chose the factor 3 as it showed the best correlation with ppmi.
    :param matrix:  numpy.2dmatrix, similarity matrix per "query words"
    :return:
    """
    m = matrix.astype(np.float64)
    m[m < 3] = 0.0
    m[m >= 3] = 1.0
    rows = (m > 0).sum(1)
    rc = rows + rows.T
    base = np.dot(m, m[:].T) * 2
    return np.divide(base, rc) * 10


def sparsity(matrix):
    """
    Calculates the sparsity of a numpy matrix/array
    :param matrix: numpy.2dmatrix, rows are "query words" and columns are "common words"
    :return:
    """
    return 1.0 - float(np.count_nonzero(matrix)) / matrix.size


def build_comp_files():
    main()


if __name__ == "__main__":
    main()
